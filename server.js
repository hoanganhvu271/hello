const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('views', path.join(__dirname, 'views'));

app.set('view engine', 'ejs');

app.get('/', async (req, res) => {
    res.render('index');
});




app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
